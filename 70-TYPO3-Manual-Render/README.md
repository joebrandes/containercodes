# TYPO3 Manual Render with Container

Die TYPO3 Dokumentation basiert auf Vorlagen in reST Technik.
Diese Verzeichnisse müssen mit genau passender **Document
Management** Umgebung *übersetzt* werden.

Man benötigt die genau passende **Sphinx** Version inklusive
aller Extensions und Abhängigkeiten - Viel Glück.

Und selbst wenn man Alles zusammengebastelt hat verlangt 
das nächste reST-Projekt andere Umgebungen und Versionen!

Wir klonen uns eine beispielhaften Dokumentations-Vorlagenordner
in unsere Arbeitsumgebung:

`git clone https://github.com/TYPO3-Documentation/TYPO3CMS-Example-ExtensionManual`

Der Rest ist dann einfach nur die Analyse der beigefügten Dateien
in Kombination mit unserem Containerwissen.

*   Welches Image rendert die Dokumentation?
*   Wie lauten die möglichen Renderbefehle in der Konsole (-> Makefile)
*   Fortgeschrittene: Welche Technik läuft im Container/Image?

Viel Spaß
