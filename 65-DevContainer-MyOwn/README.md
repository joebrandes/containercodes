# Training with Dev Containers

Wir nehmen uns einfach mal eine Umgebung wie z.B. auf 

https://github.com/devcontainers/images/tree/main/src/python/.devcontainer

beschrieben und erstellen mit VSCode hieraus unser eigenes Image.

Hier:

*   zusätzliches Python Modul: **sphinx=6.2.1**
*   zusätzliche Software: **neofetch** (demnächst dann fastfetch)

