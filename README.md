```
 _______                       _                                 _            
(_______)            _        (_)                               | |           
 _       ___  ____ _| |_ _____ _ ____  _____  ____ ____ ___   __| |_____  ___ 
| |     / _ \|  _ (_   _|____ | |  _ \| ___ |/ ___) ___) _ \ / _  | ___ |/___)
| |____| |_| | | | || |_/ ___ | | | | | ____| |  ( (__| |_| ( (_| | ____|___ |
 \______)___/|_| |_| \__)_____|_|_| |_|_____)_|   \____)___/ \____|_____|___/ 
                                                                              

```
# Container Codes

Container Stuff for my Trainees...

Ordnerstruktur (eine Idee/Vorschlag)

```
$HOME/containercodes            # GitRepo Trainer Joe Brandes
                                # Idee: Übungen in eigene Projektordner kopieren

$HOME/containerdata             # Test mit Mount Binds / Volumes 
                                # im ProdEnv sicherlich eher /var/...

$HOME/containerprojekte         # Übungen im Seminar
      -- ./dockerfile           # Unterordner: Dockerfile Übungen / Image Builds
      -- ./compose              # Unterordner: Docker Compose Übungen

```




![Container und Docker](./99-DATA/timelab-pro-sWOvgOOFk1g-unsplash.jpg "Container und Docker")

Zu Übungszwecken und Nutzung in Container-Seminaren.

[2024-05-01 : neu erstellt für Schwerpunkt Container (statt nur Docker)]

[2024-02-14 : kleinere Aktualisierungen]
