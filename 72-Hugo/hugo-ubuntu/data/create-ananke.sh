# init vars
PROJECT="quickstart"
PROJECTFOLDER="/data"
# goto folder - provided by Dockerfile: /data
cd $PROJECTFOLDER

# Create new Hugo Site with Projectname
hugo new site $PROJECT
# goto Project
cd $PROJECT
# prepare Git Env
git init -b main
# Deploy Ananke Theme via Submodule
git submodule add https://github.com/theNewDynamic/gohugo-theme-ananke.git themes/ananke

# cp prepared stuff for configuration and content from Ananke
# special config like this script provided via Docker COPY
cp $PROJECTFOLDER/config.toml $PROJECTFOLDER/quickstart
cp -a $PROJECTFOLDER/$PROJECT/themes/ananke/exampleSite/content/en $PROJECTFOLDER/$PROJECT/content/
cp -a $PROJECTFOLDER/$PROJECT/themes/ananke/exampleSite/content/fr $PROJECTFOLDER/$PROJECT/content/
rm hugo.toml

# Start the Hugo Server in a docker run Bash (or use Docker compose)
# hugo server --bind=0.0.0.0 -D
