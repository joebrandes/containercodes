# Python Docker Dev

Beispiel aus: 

* [Docs Docker - Python Develop](https://docs.docker.com/language/python/develop/)

* [GitHub Repo docker/python-docker-dev](https://github.com/docker/python-docker-dev)

Der Git Code wurde von mir geklont (Mai 2024) und aufbereitet,
da in der Anleitung auch `git init` zum Einsatz kommt.

Leider ist `git init` nur bei neueren Docker Desktop verfügbar!
