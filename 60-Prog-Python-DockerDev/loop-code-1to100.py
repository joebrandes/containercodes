# Initialisiere die Zählvariable
count = 1

# Äußere Schleife für die Zeilenumbrüche
for row in range(1, 11):
    # Innere Schleife für die Ausgabe von 10 Zahlen pro Zeile
    for col in range(1, 11):
        # Gib die aktuelle Zahl aus und erhöhe den Zähler
        print(f"{count:2d}", end=" ")
        count += 1
    # Zeilenumbruch nach 10 Zahlen
    print()

