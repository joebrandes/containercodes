# Unterlagen für TN Containerseminare Joe Brandes

![Container und Docker](../99-DATA/timelab-pro-sWOvgOOFk1g-unsplash.jpg "Container und Docker")

RST-Unterlage Online (HTML, Singlehtml, PDF)

*   [Seminar Container Trainer Joe Brandes](http://container.joe-brandes.de/ "Seminarunterlage")

Seminarbeiträge auf Website: 

*   [PC Systembetreuer Portal](http://pcsystembetreuer.de)

*   [Fachkraft IT-Systeme und -Netzwerke](http://fitsn.de)


Dieses Gitlab Repo

*   [Containercodes auf Joe Brandes Gitlab Repo](https://gitlab.com/joebrandes/containercodes.git "Containercodes

Externe Beispiele für Container / Docker / Docker Compose und Co

*   [Dockerbuch Öggl und Kofler / Rheinwerk Verlag](https://github.com/docbuc "Dockerbuch")
*   [Docker Github Repo Awesome Compose](https://github.com/docker/awesome-compose "Docker Awesome Compose")
